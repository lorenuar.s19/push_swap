# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/04/10 13:37:24 by lorenuar          #+#    #+#              #
#    Updated: 2021/06/25 13:22:17 by lorenuar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# ================================ VARIABLES ================================= #

# The name of your executable
NAME	= libcommon.a

# Compiler and compiling flags
CC	= gcc
CFLAGS	= -Wall -Werror -Wextra

AR = ar
ARFLAGS = -rcs

# Debug, use with`make DEBUG=1`
ifeq ($(DEBUG),1)
CFLAGS	+= -g3
endif
ifeq ($(DEBUG),2)
CFLAGS	+= -g3 -fsanitize=address
endif
ifeq ($(DEBUG),3)
CFLAGS	+= -g3 -fsanitize=address -fsanitize=leak
endif

# Folder name
SRCDIR	= src/
INCDIR	= includes/
OBJDIR	= bin/

FLD_LIB = ../lib_utils
BIN_LIB = -lutils
INC_LIB = $(FLD_LIB)/includes

# Add include folder
CFLAGS	+= -I $(INCDIR)
CFLAGS	+= -I $(INC_LIB)

# Linking stage flags
LDFLAGS = -L $(FLD_LIB) $(BIN_LIB)

###▼▼▼<src-updater-do-not-edit-or-remove>▼▼▼
# **************************************************************************** #
# **   Generated with https://github.com/lorenuars19/makefile-src-updater   ** #
# **************************************************************************** #

SRCS = \
	./src/stacks_free_all.c \
	./src/parse_opts.c \
	./src/sub_print_instr.c \
	./src/print_pos_table.c \
	./src/instr/read_instrs.c \
	./src/instr/i_push.c \
	./src/instr/i_revrot.c \
	./src/instr/i_swap.c \
	./src/instr/exec_instr.c \
	./src/instr/i_rot.c \
	./src/print_stacks_draw.c \
	./src/stack_check_valid.c \
	./src/print_stacks.c \
	./src/stacks_parse_args.c \
	./src/populate_pos_table.c \
	./src/setup_pos_table.c \
	./src/stack_check_sorted.c \

HEADERS = \
	./includes/libcommon.h \

###▲▲▲<src-updater-do-not-edit-or-remove>▲▲▲

# String manipulation magic
SRC		:= $(notdir $(SRCS))
OBJ		:= $(SRC:.c=.o)
OBJS	:= $(addprefix $(OBJDIR), $(OBJ))

# Colors
GR	= \033[32;1m
RE	= \033[31;1m
YE	= \033[33;1m
CY	= \033[36;1m
RC	= \033[0m

# Implicit rules
VPATH := $(SRCDIR) $(OBJDIR) $(shell find $(SRCDIR) -type d)

# ================================== RULES =================================== #

# This specifies the rules that does not correspond to any filename
.PHONY	= all run clean fclean re

all : $(OBJDIR) $(NAME)

$(OBJDIR) :
	@mkdir -p $(OBJDIR)

# Compiling
$(OBJDIR)%.o : %.c
	@$(CC) $(CFLAGS) -c $^ -o $@
	@printf "$(GR)+$(RC)"

# Archiving
$(NAME)	: $(SRCS) $(HEADERS) $(OBJS)
	@printf "\n$(GR)=== Compiled [$(CC) $(CFLAGS)] ===\n--- $(SRC)$(RC)\n"
	@rm -f $(NAME)
	@$(AR) $(ARFLAGS) $(NAME) $(OBJS)
	@printf "$(YE)&&& Archived [$(AR) $(ARFLAGS)] &&&\n--- $(NAME)$(RC)\n"

# Cleaning
clean :
	@printf "$(RE)--- Removing $(OBJDIR)$(RC)\n"
	@rm -rf $(OBJDIR)

fclean : clean
	@printf "$(RE)--- Removing $(NAME)$(RC)\n"
	@rm -f $(NAME)

# Special rule to force to remake everything
re : fclean all

test : all
	rm -f test.exec
	$(CC) $(CFLAGS) main.c libutils.a -o test.exec
ifeq ($(DEBUG),2)
		-gdb --args ./test.exec main.c
endif
	-./test.exec
	rm -f test.exec
