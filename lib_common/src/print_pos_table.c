/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_pos_table.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/22 09:22:59 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/30 13:36:46 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

static void	sub_print_sorted(t_sdat *sd, int i)
{
	if (i >= 0 && i < sd->inp_siz)
	{
		if (sd->postab[i].sorted)
		{
			ft_printf("\033[32;1mSORTED\033[0m");
		}
		else
		{
			ft_printf("\033[31;1m\033[47mGOING TO SPLIT\033[0m");
		}
	}
}

static void	sub_print_pos(t_sdat *sd, int ia, int ib)
{
	if (ia >= 0 && ia < sd->siz.a)
	{
		ft_printf("[i %4d] num %8d | pos %4d | sorted",
			ia, sd->postab[ia].number, sd->postab[ia].pos);
		sub_print_sorted(sd, ia);
	}
	if (ib >= sd->siz.a && ib < sd->inp_siz)
	{
		ft_printf(ABSO_OFFS " | [i %4d] num %8d | pos %4d | sorted",
			ib, sd->postab[ib].number, sd->postab[ib].pos);
		sub_print_sorted(sd, ib);
	}
}

void	print_pos_table(t_sdat *sd)
{
	int	ia;
	int	ib;

	ia = 0;
	ib = sd->siz.a;
	ft_printf("=== POS TABLE  === size A %d " ABSO_OFFS " | = size B %d ===\n",
		sd->siz.a, sd->siz.b);
	while ((ia >= 0 && ia < sd->siz.a && ia < sd->inp_siz)
		|| (ib >= sd->siz.a && ib < sd->inp_siz))
	{
		sub_print_pos(sd, ia, ib);
		put_char('\n', 1);
		ia++;
		ib++;
	}
}
