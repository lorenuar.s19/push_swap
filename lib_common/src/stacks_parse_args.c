/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stacks_parse_args.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 15:54:14 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/25 12:03:55 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

int	stacks_parse_args(int argc, char *argv[], t_stack *a, int verbose)
{
	int	offs;

	offs = 1;
	if (verbose)
	{
		offs = 2;
	}
	if (argc == 2 || (verbose && argc == 3))
	{
		if (stack_from_str(argv[offs], a))
			return (1);
	}
	else if (argc > 2)
	{
		if (stack_from_args(argc, argv, a, offs))
			return (1);
	}
	if (a && !a->dat)
		return (error_put(1, E_SPS0));
	return (0);
}
