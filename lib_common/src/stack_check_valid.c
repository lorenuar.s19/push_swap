/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_check_valid.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/19 14:07:08 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/21 23:47:08 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "libcommon.h"

int	stack_check_valid(t_stack *sta)
{
	int		dup;
	int		i;
	int		j;

	if (!sta)
		return (error_put(1, E_SCV0));
	i = 0;
	while (sta && sta->dat && i < sta->siz)
	{
		dup = 0;
		if (sta->dat[i] > INT_MAX || sta->dat[i] < INT_MIN)
			return (error_put(2, E_SCV1));
		j = i;
		while (sta && sta->dat && j < sta->siz)
		{
			if (sta->dat[i] == sta->dat[j])
				dup++;
			j++;
		}
		if (dup != 1)
			return (1);
		i++;
	}
	return (0);
}
