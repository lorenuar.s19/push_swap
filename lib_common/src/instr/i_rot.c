/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_rot.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 22:06:05 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/21 22:09:51 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

int	i_rot_a(t_stack *sta, t_stack *stb)
{
	(void)stb;
	if (!sta || !sta->dat || sta->siz < 2)
		return (0);
	return (stack_rotate(sta));
}

int	i_rot_b(t_stack *sta, t_stack *stb)
{
	(void)sta;
	if (!stb || !stb->dat || stb->siz < 2)
		return (0);
	return (stack_rotate(stb));
}

int	i_rot_both(t_stack *sta, t_stack *stb)
{
	if (!sta || !sta->dat || !stb || !stb->dat)
		return (0);
	return (i_rot_a(sta, stb) || i_rot_b(sta, stb));
}
