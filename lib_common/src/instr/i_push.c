/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   i_push.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 22:06:05 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/22 10:07:50 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

int	i_push_a(t_stack *sta, t_stack *stb)
{
	if (!stb && !stb->dat)
		return (0);
	if (stack_push(sta, stb->dat[0]))
		return (1);
	if (stack_pop(stb))
		return (1);
	return (0);
}

int	i_push_b(t_stack *sta, t_stack *stb)
{
	if (!sta && sta->dat)
		return (0);
	if (stack_push(stb, sta->dat[0]))
		return (1);
	if (stack_pop(sta))
		return (1);
	return (0);
}
