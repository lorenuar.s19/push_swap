/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setup_pos_table.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/27 23:09:25 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/25 13:22:32 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <limits.h>
#include "libcommon.h"

static int	create_pos_table(t_ptab **itab, ssize_t siz_postab)
{
	ssize_t	i;

	if (!itab)
		return (1);
	(*itab) = malloc(siz_postab * sizeof(t_ptab));
	i = 0;
	while (i < siz_postab)
	{
		(*itab)[i].pos = -1;
		(*itab)[i].number = 0;
		(*itab)[i].sorted = 0;
		i++;
	}
	return (0);
}

int	setup_pos_table(t_sdat *sd)
{
	if (create_pos_table(&(sd->postab), sd->inp_siz))
		return (error_put(1, E_SIT0));
	if (populate_pos_table(sd))
		return (error_put(1, E_SIT2));
	return (0);
}

int	update_pos_table(t_sdat *sd)
{
	ssize_t	i;

	sd->siz.a = 0;
	if (sd->sta && sd->sta->dat)
	{
		sd->siz.a = stack_get_size(sd->sta);
	}
	sd->siz.b = 0;
	if (sd->stb && sd->stb->dat)
	{
		sd->siz.b = stack_get_size(sd->stb);
	}
	i = 0;
	while (i < sd->inp_siz)
	{
		sd->postab[i].pos = -1;
		i++;
	}
	if (populate_pos_table(sd))
		return (error_put(1, E_UPT0));
	return (0);
}
