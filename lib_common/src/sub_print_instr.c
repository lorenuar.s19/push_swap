/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sub_print_instr.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 17:49:45 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/22 10:42:59 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

void	sub_print_instr(int instr)
{
	static char	*str_instr_desc[N_INSTR] = {
		"swap A", "swap B", "swap A & B",
		"push A", "push B",
		"rotate A", "rotate A", "rotate A & B",
		"reverse rotate A", "reverse rotate B", "reverse rotate A & B"};

	if (instr == I_NVALID)
	{
		put_str("\n");
		return ;
	}
	ft_printf(" EXEC : %s : ", str_instr_desc[instr]);
	if (instr == I_PA || instr == I_PB)
	{
		ft_printf("\033[4%dm   \033[0m = NEW", COL1);
	}
	else
	{
		ft_printf("\033[4%dm   \033[0m moved from \033[4%dm   \033[0m", COL2, COL3);
	}
	ft_printf("\n");
}
