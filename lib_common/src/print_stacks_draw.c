/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_stacks_draw.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 17:49:45 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/25 17:47:58 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

static int	get_color_a(t_pdata *pdat, int ins)
{
	int	i;

	i = pdat->ia;
	if (i == 0 && (ins == I_PA))
		return (COL1);
	if (i == 0 && (ins == I_RA || ins == I_SA || ins == I_RR || ins == I_SS))
		return (COL3);
	if (i == 1 && (ins == I_SA || ins == I_SS))
		return (COL2);
	if (i == pdat->sta->siz - 1 && (ins == I_RA || ins == I_RR))
		return (COL2);
	if (i == 0 && (ins == I_RRA || ins == I_RRR))
		return (COL2);
	if (i == pdat->sta->siz - 1 && (ins == I_RRA || ins == I_RRR))
		return (COL3);
	return (7);
}

static int	get_color_b(t_pdata *pdat, int ins)
{
	int	i;

	i = pdat->ib;
	if (i == pdat->sta->siz && (ins == I_PB))
		return (COL1);
	if (i == pdat->sta->siz
		&& (ins == I_RB || ins == I_SB || ins == I_RR || ins == I_SS))
		return (COL3);
	if (i == pdat->sta->siz + 1 && (ins == I_SB || ins == I_SS))
		return (COL2);
	if (i == (pdat->sta->siz + pdat->stb->siz) - 1
		&& (ins == I_RB || ins == I_RR))
		return (COL2);
	if (i == pdat->sta->siz && (ins == I_RRB || ins == I_RRR))
		return (COL2);
	if (i == (pdat->sta->siz + pdat->stb->siz) - 1
		&& (ins == I_RRB || ins == I_RRR))
		return (COL3);
	return (7);
}

static void	sub_print_stacks(t_pdata *pdat, int maxsiz, t_sdat *sd, int ins)
{
	static int	ret = 0;
	int			a_printed;
	int			col;

	a_printed = 0;
	if (pdat->ia >= 0 && pdat->ia < sd->siz.a && pdat->ia < sd->inp_siz)
	{
		a_printed = 1;
		col = get_color_a(pdat, ins);
		ret = ft_printf("\033[0m\033[3%d;1mA %8d \033[4%d;1m", \
			col, sd->postab[pdat->ia].number, col);
		put_str_times(" ", 1 + sd->postab[pdat->ia].pos, 1);
	}
	if (!a_printed)
		ft_printf("\033[0m\033[%dG", (maxsiz + 2) + ret);
	else
		ft_printf("\033[0m\033[%dG", (maxsiz + 2) + ret);
	if (pdat->ib >= sd->siz.a && pdat->ib < sd->inp_siz)
	{
		col = get_color_b(pdat, ins);
		ft_printf("| \033[3%d;1mB %8d \033[4%d;1m", \
			col, sd->postab[pdat->ib].number, col);
		put_str_times(" ", sd->postab[pdat->ib].pos, 1);
		ft_printf("\033[0m");
	}
}

static void	sub_print_header(t_pdata *pdat, int maxsiz, t_sdat *sd, int instr)
{
	pdat->pad_n = ft_printf("= - POS -");
	pdat->pad_b = ft_printf(" A size %d ", sd->siz.a) - 2;
	ft_printf("%*s- B size %d =", maxsiz + 10, "", sd->siz.b);
	sub_print_instr(instr);
}

void	print_stacks_draw(t_stack *a, t_stack *b, int instr, t_sdat *sd)
{
	static int	maxsiz = 0;
	t_pdata		pdat;
	int			wait;

	if (sd->verbose == 0 || (!a && !b) || update_pos_table(sd))
		return ;
	pdat = (t_pdata){a, b, 0, sd->siz.a, sd->siz.a, sd->siz.b, 0, 0};
	if (pdat.siz_a > maxsiz || pdat.siz_b > maxsiz)
		maxsiz = maxi(pdat.siz_a, pdat.siz_b);
	if (maxsiz >= 20)
		put_str("\033[2J");
	sub_print_header(&pdat, maxsiz, sd, instr);
	while (pdat.ia < sd->siz.a || pdat.ib < sd->inp_siz)
	{
		ft_printf("\033[0m%7d > ", pdat.ia);
		sub_print_stacks(&pdat, maxsiz, sd, instr);
		put_str("\n");
		pdat.ia++;
		pdat.ib++;
	}
	while (pdat.ia <= maxsiz)
		ft_printf("\033[0m%7d > \n", pdat.ia++);
	wait = 0;
	while (wait < ((10000000) + (DELAY * 10000000)))
		wait++;
}
