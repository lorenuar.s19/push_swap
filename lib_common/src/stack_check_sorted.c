/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_check_sorted.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/19 14:07:08 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/22 00:35:04 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

int	stack_check_sorted_ascending(t_stack *sta)
{
	int	i;
	int	diff;

	if (!sta)
		return (0);
	diff = 0;
	i = 0;
	while (sta && sta->dat && i < sta->siz - 1)
	{
		if (sta->dat[i] > sta->dat[i + 1])
		{
			diff++;
		}
		i++;
	}
	if (diff)
	{
		return (diff);
	}
	return (diff);
}

int	stack_check_sorted_descending(t_stack *sta)
{
	int	i;
	int	diff;

	if (!sta)
		return (0);
	diff = 0;
	i = 0;
	while (sta && sta->dat && i < sta->siz - 1)
	{
		if (sta->dat[i] < sta->dat[i + 1])
		{
			diff++;
		}
		i++;
	}
	if (diff)
	{
		return (diff);
	}
	return (diff);
}
