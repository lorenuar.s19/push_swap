/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_stacks.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/17 17:49:45 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/25 17:51:33 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

static int	get_color_a(t_pdata *pdat, int ins)
{
	int	i;

	i = pdat->ia;
	if (i == 0 && (ins == I_PA))
		return (COL1);
	if (i == 0 && (ins == I_RA || ins == I_SA || ins == I_RR || ins == I_SS))
		return (COL3);
	if (i == 1 && (ins == I_SA || ins == I_SS))
		return (COL2);
	if (i == pdat->sta->siz - 1 && (ins == I_RA || ins == I_RR))
		return (COL2);
	if (i == 0 && (ins == I_RRA || ins == I_RRR))
		return (COL2);
	if (i == pdat->sta->siz - 1 && (ins == I_RRA || ins == I_RRR))
		return (COL3);
	return (7);
}

static int	get_color_b(t_pdata *pdat, int ins)
{
	int	i;

	i = pdat->ib;
	if (i == 0 && (ins == I_PB))
		return (COL1);
	if (i == 0 && (ins == I_RB || ins == I_SB || ins == I_RR || ins == I_SS))
		return (COL3);
	if (i == 1 && (ins == I_SB || ins == I_SS))
		return (COL2);
	if (i == pdat->stb->siz - 1 && (ins == I_RB || ins == I_RR))
		return (COL2);
	if (i == 0 && (ins == I_RRB || ins == I_RRR))
		return (COL2);
	if (i == pdat->stb->siz - 1 && (ins == I_RRB || ins == I_RRR))
		return (COL3);
	return (7);
}

static void	sub_print_stacks(t_pdata *pdat, int ins)
{
	static int	ret = 0;
	int			a_printed;
	int			col;

	a_printed = 0;
	col = 7;
	if (pdat->ia >= 0 && pdat->ia < pdat->sta->siz)
	{
		col = get_color_a(pdat, ins);
		ret = ft_printf("\033[0m\033[3%d;1mA %4d\033[0m", \
			col, pdat->sta->dat[pdat->ia], col);
		a_printed = 1;
	}
	if (pdat->siz_b)
	{
		if (a_printed == 0)
			ft_printf("%*s", ret, "");
		ft_printf(" \033[20G| ");
	}
	if (pdat->ib >= 0 && pdat->ib < pdat->stb->siz)
	{
		col = get_color_b(pdat, ins);
		ft_printf("\033[3%d;1mB %4d\033[0m", col, pdat->stb->dat[pdat->ib]);
	}
}

void	print_stacks(t_stack *a, t_stack *b, int instr, int verbose)
{
	t_pdata	pdat;

	if (verbose == 0 || (!a && !b))
		return ;
	pdat = (t_pdata){a, b, 0, 0, a->siz, b->siz, 0, 0};
	pdat.pad_n = ft_printf("= - POS -");
	pdat.pad_b = ft_printf(" A size %d ", pdat.siz_a) - 2;
	ft_printf("- B size %d =", pdat.siz_b);
	sub_print_instr(instr);
	while (pdat.ia < a->siz || pdat.ib < b->siz)
	{
		ft_printf("%7d > ", pdat.ia);
		sub_print_stacks(&pdat, instr);
		if (verbose == 2 && (pdat.ia >= SHORT_MAX_NODES - 1
				|| pdat.ib >= SHORT_MAX_NODES - 1))
		{
			ft_printf("\t ... SHORTENED ... MAX %02d ...\n", SHORT_MAX_NODES);
			return ;
		}
		put_str("\n");
		pdat.ia++;
		pdat.ib++;
	}
	put_str("\n");
}
