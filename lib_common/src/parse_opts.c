/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_opts.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/24 15:08:43 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/22 10:53:54 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libcommon.h"

int	parse_opts(char argc, char *argv[])
{
	int	i;

	i = 0;
	while (i < argc && argv && argv[i])
	{
		if (str_cmp_n("-v", argv[i], 3) == 0)
			return (1);
		if (str_cmp_n("-vs", argv[i], 4) == 0)
			return (2);
		if (str_cmp_n("-vd", argv[i], 4) == 0)
			return (3);
		i++;
	}
	return (0);
}
