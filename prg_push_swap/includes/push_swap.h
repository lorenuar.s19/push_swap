/* ********
****************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/10 13:38:11 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/24 19:03:50 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "libutils.h"
# include "libcommon.h"

typedef struct s_find_correct_place
{
	int		pre;
	int		cur;
	int		nex;

	t_range	a;
	t_range	b;

	t_range	all;
}	t_fcp;

typedef struct s_mark_to_split_data
{
	int		pos;

	int		min;
	int		max;

	int		min_pos;
	int		max_pos;

	int		i;

}			t_mts;

typedef struct s_scan_for_best_move_data
{
	t_pair	pos;
	t_pair	moves;
	t_pair	best_moves;
	t_range	best_pos;
	t_range	best;
	int		total_moves;
	int		best_total_moves;
}	t_bmdat;

typedef struct s_find_best_place_data_bundle
{
	t_bmdat	*bd;
	t_fcp	*fcp;
}			t_fbp;

typedef struct s_best_stack
{
	t_stack	*best;
	long	len;
	long	index;
}	t_best;

typedef struct s_build_inc_seq_data
{
	int		start_visited;
	int		sub_start_visited;
	int		start_i;
	int		i;
	int		j;

}	t_bis;

void	print_instr(int instr);
int		exec_print_instr(t_sdat *sd, int instr);
int		generate_instr(t_stack *a, t_stack *b, int verbose);
int		compute_instr(t_sdat *sd);

void	free_sorter_data(t_sdat *sd);
t_range	*create_range_table(t_sdat *sd);
void	pos_table_reset_split(t_sdat *sd, int val);

int		merge_back(t_sdat *sd);
int		exec_rotate(t_sdat *sd, t_bmdat *bd);
int		exec_push(t_sdat *sd);
int		check_pre_sorted(t_sdat *sd);
int		put_zero_on_top(t_sdat *sd);

int		can_insert(t_sdat *sd, t_bmdat *bd);
void	find_best_place(t_sdat *sd, t_bmdat *bd, t_fcp *fcp);

int		get_index_num(t_sdat *sd, ssize_t num);
int		get_index_pos(t_sdat *sd, ssize_t num);

int		get_pos(t_sdat *sd, ssize_t num);
int		get_min(int a, int b);

int		get_pos_sorted(t_sdat *sd, ssize_t num);
int		has_notsorted(t_sdat *sd, t_stack *stack);
int		mark_to_split(t_sdat *sd, t_stack *lis);
int		split_to_b(t_sdat *sd, t_stack *lis);
int		update_split(t_sdat *sd, t_stack *lis);
int		copy_stack(t_stack *src, t_stack *dest);
int		find_minimum(t_sdat *sd);
int		build_inc_sequence(t_sdat *sd, t_stack *stack, int offs);

/*
** Errors
*/
# define E_CI0      "compute_instr : mark_to_split()"
# define E_CI1      "compute_instr : split_to_b()"
# define E_CI2      "compute_instr : scan_for_best_move()"
# define E_CRT0     "create_range_table : NULL pointer from malloc()"
# define E_CRT1     "create_range_table : Trying to divide by ZERO"
# define E_GI0      "generate_instr : init_sorter_data()"
# define E_GI1      "generate_instr : compute_instr()"

# define E_ISD0     "init_sorter_data : create_range_table()"
# define E_ISD1     "init_sorter_data : setup_pos_tables()"
# define E_MTS0     "mark_to_split : pos invalid"
# define E_MTS1     "mark_to_split : index invalid"
# define E_PA0      "parse_args : stack_from_str()"
# define E_PA1      "parse_args : stack_from_args()"
# define E_PS0      "push_swap : parse_args()"
# define E_PS1      "push_swap : input is invalid"
# define E_PS2      "push_swap : non zero exit code from generate_instr()"

# define E_STB0     "split_to_b : exec_print_instr() push"
# define E_STB1     "split_to_b : exec_print_instr() rotate"
# define E_STB3     "split_to_b : sub_sort_three()"

# define E_ .

#endif
