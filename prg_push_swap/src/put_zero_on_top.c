/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_zero_on_top.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/12 20:56:04 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/21 23:17:09 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	sub_move_on_top(t_sdat *sd, int i)
{
	while (i)
	{
		if (i > 0)
		{
			if (exec_print_instr(sd, I_RA))
				return (1);
			i--;
		}
		else if (i < 0)
		{
			if (exec_print_instr(sd, I_RRA))
				return (1);
			i++;
		}
	}
	return (0);
}

int	put_zero_on_top(t_sdat *sd)
{
	t_stack	*stack;
	int		i;

	stack = NULL;
	if (sd->sta && sd->sta->dat)
		stack = sd->sta;
	i = 0;
	sd->siz.a = 0;
	if (sd->sta && sd->sta->dat)
		sd->siz.a = stack_get_size(sd->sta);
	while (i < sd->siz.a && i >= 0)
	{
		if (get_pos(sd, stack->dat[i]) == 0)
			break ;
		i++;
	}
	if (i > sd->siz.a / 2)
		i = i - sd->siz.a;
	if (sub_move_on_top(sd, i))
		return (1);
	return (0);
}
