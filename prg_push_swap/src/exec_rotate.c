/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_rotate.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/12 20:56:04 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/22 10:56:03 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	sub_rotate_a(t_sdat *sd, t_bmdat *bd)
{
	if (sd->sta && sd->sta->siz < 2)
	{
		return (0);
	}
	if (bd->best_moves.a > 0)
	{
		if (exec_print_instr(sd, I_RA))
			return (1);
		bd->best_moves.a--;
	}
	if (bd->best_moves.a < 0)
	{
		if (exec_print_instr(sd, I_RRA))
			return (1);
		bd->best_moves.a++;
	}
	return (0);
}

static int	sub_rotate_b(t_sdat *sd, t_bmdat *bd)
{
	if (sd->stb && sd->stb->siz < 2)
	{
		return (0);
	}
	if (bd->best_moves.b > 0)
	{
		if (exec_print_instr(sd, I_RB))
			return (1);
		bd->best_moves.b--;
	}
	if (bd->best_moves.b < 0)
	{
		if (exec_print_instr(sd, I_RRB))
			return (1);
		bd->best_moves.b++;
	}
	return (0);
}

static int	sub_rotate_both(t_sdat *sd, t_bmdat *bd)
{
	if (bd->best_moves.a > 0 && bd->best_moves.b > 0)
	{
		if (exec_print_instr(sd, I_RR))
			return (error_put(1, "sub_align_push : exec_print_instr() I_RR"));
		bd->best_moves.a--;
		bd->best_moves.b--;
	}
	if (bd->best_moves.a < 0 && bd->best_moves.b < 0)
	{
		if (exec_print_instr(sd, I_RRR))
			return (error_put(1, "sub_align_push : exec_print_instr() I_RRR"));
		bd->best_moves.a++;
		bd->best_moves.b++;
	}
	return (0);
}

static void	sub_set_best_moves(t_sdat *sd, t_bmdat *bd)
{
	if (bd->best_moves.a == sd->siz.a
		|| (sd->siz.a > 3 && bd->best_moves.a > sd->siz.a / 2))
	{
		bd->best_moves.a -= sd->siz.a;
	}
	if (bd->best_moves.b == sd->siz.b
		|| (sd->siz.b > 3 && bd->best_moves.b > sd->siz.b / 2))
	{
		bd->best_moves.b -= sd->siz.b;
	}
}

int	exec_rotate(t_sdat *sd, t_bmdat *bd)
{
	sub_set_best_moves(sd, bd);
	while (sd && (bd->best_moves.a || bd->best_moves.b))
	{
		if ((bd->best_moves.a > 0 && bd->best_moves.b > 0)
			|| (bd->best_moves.a < 0 && bd->best_moves.b < 0))
		{
			if (sd->sta && sd->stb && sub_rotate_both(sd, bd))
				return (1);
		}
		else if (bd->best_moves.a > 0 || bd->best_moves.b > 0
			|| bd->best_moves.a < 0 || bd->best_moves.b < 0)
		{
			if (sub_rotate_a(sd, bd))
				return (1);
			if (sub_rotate_b(sd, bd))
				return (1);
		}
	}
	return (0);
}
