/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_sorter_data.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/04 18:31:00 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/30 13:37:52 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "push_swap.h"

void	free_sorter_data(t_sdat *sd)
{
	if (sd && sd->range_table)
	{
		free(sd->range_table);
		sd->range_table = NULL;
	}
	if (sd && sd->postab)
	{
		free(sd->postab);
		sd->postab = NULL;
	}
}
