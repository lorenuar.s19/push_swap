/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   build_inc_sequence.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 14:53:01 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/30 14:13:26 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	sub_find_next_inc(t_sdat *sd, t_stack *stack, t_bis *b)
{
	b->sub_start_visited = 0;
	while (b->j >= 0 && b->j < sd->inp_siz
		&& sd->postab[b->i].pos >= sd->postab[b->j].pos)
	{
		if (b->j == b->start_i)
			b->sub_start_visited++;
		if (b->sub_start_visited > 1)
			break ;
		b->j++;
		if (b->j >= sd->inp_siz)
			b->j = 0;
	}
	if (b->j >= 0 && b->j < sd->inp_siz && b->j != b->start_i
		&& stack_push_back(stack, sd->postab[b->j].pos))
		return (error_put(1, "build_inc_sequence : stack_push_back()"));
	b->i = b->j;
	return (0);
}

int	build_inc_sequence(t_sdat *sd, t_stack *stack, int offs)
{
	t_bis	b;

	b = (t_bis){0, 0, offs, offs, 0};
	if (b.i >= 0 && b.i < sd->inp_siz
		&& stack_push_back(stack, sd->postab[b.i].pos))
		return (error_put(1, "build_inc_sequence : stack_push_back()"));
	while (b.i >= 0 && b.i < sd->inp_siz)
	{
		if (b.i == b.start_i)
			b.start_visited++;
		if (b.start_visited > 1)
			break ;
		b.j = b.i;
		if (sub_find_next_inc(sd, stack, &b))
			return (error_put(1, "build_inc_sequence : sub_find_next_inc()"));
		if (b.i >= sd->inp_siz)
			b.i = 0;
	}
	return (0);
}
