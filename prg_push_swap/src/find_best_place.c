/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_best_place.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 14:53:01 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/24 18:40:59 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	sub_set(int *set1, int val1, int *set2, int val2)
{
	if (set1)
	{
		*set1 = val1;
	}
	if (set2)
	{
		*set2 = val2;
	}
}

static void	sub_best_place(t_fbp *fbp, int pos, int i)
{
	t_bmdat	*bd;
	t_fcp	*fcp;

	fcp = fbp->fcp;
	bd = fbp->bd;
	if (fcp->cur == fcp->all.min)
	{
		if ((fcp->cur == fcp->all.min) && pos >= bd->best.max)
			sub_set(&(bd->best.max), pos, &(bd->best_pos.max), i);
		else if ((fcp->cur == fcp->all.min) && pos <= bd->best.min && pos > 0)
			sub_set(&(bd->best.min), pos, &(bd->best_pos.min), i);
	}
	else if (fcp->cur == fcp->all.max)
	{
		if ((fcp->cur == fcp->all.max) && pos > bd->best.max && pos < fcp->cur)
			sub_set(&(bd->best.max), pos, &(bd->best_pos.max), i);
		if ((fcp->cur == fcp->all.max) && (pos == fcp->a.min || (pos == 0)))
			sub_set(&(bd->best.min), pos, &(bd->best_pos.min), i);
	}
	if ((pos > 0 && pos < fcp->all.max) && pos > bd->best.max && pos < fcp->cur)
		sub_set(&(bd->best.max), pos, &(bd->best_pos.max), i);
	else if ((pos > 0 && pos < fcp->all.max)
		&& (pos < bd->best.min && pos > fcp->cur))
		sub_set(&(bd->best.min), pos, &(bd->best_pos.min), i);
}

void	find_best_place(t_sdat *sd, t_bmdat *bd, t_fcp *fcp)
{
	t_fbp	fbp;
	int		pos;
	int		i;

	fbp = (t_fbp){bd, fcp};
	bd->best_pos = (t_range){-1, -1};
	bd->best = (t_range){sd->inp_siz, 0};
	i = 0;
	while (i >= 0 && i < sd->siz.a)
	{
		pos = sd->postab[i].pos;
		sub_best_place(&fbp, pos, i);
		i++;
	}
}
