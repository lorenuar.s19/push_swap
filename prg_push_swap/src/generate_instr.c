/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   generate_instr.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 19:46:15 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/22 10:53:18 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	init_sorter_data(t_sdat *sd)
{
	if (!create_range_table(sd))
	{
		return (error_put(1, E_ISD0));
	}
	sd->inp_siz = sd->siz.a;
	if (setup_pos_table(sd))
	{
		return (error_put(1, E_ISD1));
	}
	return (0);
}

int	generate_instr(t_stack *sta, t_stack *stb, int verbose)
{
	t_sdat	sd;

	sd = (t_sdat){sta, stb, NULL, (t_range){0, 0}, 0,
		NULL, (t_pair){0, 0}, 0, 0, 0, verbose};
	if (stack_check_sorted_ascending(sta) == 0)
	{
		return (0);
	}
	if (init_sorter_data(&sd))
	{
		free_sorter_data(&sd);
		return (error_put(1, E_GI0));
	}
	if (compute_instr(&sd))
	{
		free_sorter_data(&sd);
		return (error_put(1, E_GI1));
	}
	free_sorter_data(&sd);
	return (0);
}
