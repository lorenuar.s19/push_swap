/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/10 13:37:56 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/22 17:39:37 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include <stdio.h>

void	print_instr(int instr)
{
	static char	*str_instrs[N_INSTR] = {
	"sa", "sb", "ss", "pa", "pb", "ra", "rb", "rr", "rra", "rrb", "rrr"};

	if (instr >= 0 && instr < N_INSTR)
	{
		put_str_nl(str_instrs[instr]);
	}
}

static int	push_swap(int argc, char *argv[], int verbose)
{
	t_stack	a;
	t_stack	b;

	a = (t_stack){NULL, 0, 0};
	b = (t_stack){NULL, 0, 0};
	if (stacks_parse_args(argc, argv, &a, verbose))
	{
		stacks_free_all(&a, &b);
		return (error_put(1, E_PS0));
	}
	if (stack_check_valid(&a))
	{
		stacks_free_all(&a, &b);
		return (error_put(1, E_PS1));
	}
	print_stacks(&a, &b, I_NVALID, verbose);
	if (generate_instr(&a, &b, verbose))
	{
		stacks_free_all(&a, &b);
		return (error_put(1, E_PS2));
	}
	stacks_free_all(&a, &b);
	return (0);
}

int	main(int argc, char *argv[])
{
	if (argc >= 2)
	{
		return (push_swap(argc, argv, parse_opts(argc, argv)));
	}
	return (0);
}
