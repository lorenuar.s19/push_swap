/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_push.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/12 20:56:04 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/24 18:38:32 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	exec_push(t_sdat *sd)
{
	if ((sd->sta && !sd->sta->dat) || (sd->stb && !sd->stb->dat))
	{
		return (error_put(1, "exec_push : invalid input"));
	}
	if (exec_print_instr(sd, I_PA))
		return (error_put(1, "exec_push : exec_print_instr()"));
	return (0);
}
