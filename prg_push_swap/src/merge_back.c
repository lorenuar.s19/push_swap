/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   merge_back.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/12 20:56:04 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/24 18:40:05 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

// #define NODBG //TODO REMOVE BEFORE PUSH

#include "push_swap.h"

static int	select_best_move(t_sdat *sd, t_bmdat *bd)
{
	t_pair	tmp;

	if (can_insert(sd, bd))
		return (error_put(1, "select_best_move : can_insert()"));
	if (sd->push_type)
		sd->best_push_type = sd->push_type;
	tmp = bd->moves;
	if (tmp.a < 0)
		tmp.a = -tmp.a;
	if (tmp.b < 0)
		tmp.b = -tmp.b;
	bd->total_moves = tmp.a + tmp.b;
	if (bd->total_moves < bd->best_total_moves)
	{
		bd->best_total_moves = bd->total_moves;
		bd->best_moves = bd->moves;
	}
	return (0);
}

static int	scan_for_best_move(t_sdat *sd, t_bmdat *bd)
{
	bd->best_moves = sd->siz;
	sd->best_push_type = 0;
	bd->pos.b = sd->siz.a;
	bd->best_total_moves = ~(1 << 31);
	while (sd->stb && sd->stb->siz && bd->pos.b < sd->inp_siz
		&& bd->pos.b - sd->siz.a >= 0 && bd->pos.b - sd->siz.a < sd->siz.b / 2
		&& bd->best_total_moves > 0)
	{
		bd->pos.a = 0;
		if (select_best_move(sd, bd))
			return (error_put(1, "scan_for_best_move : select_best_move()"));
		bd->pos.b++;
	}
	bd->pos.b = sd->inp_siz - 1;
	while (sd->stb && sd->stb->siz && bd->pos.b < sd->inp_siz
		&& bd->pos.b - sd->siz.a >= sd->siz.b / 2 && bd->pos.b < sd->inp_siz
		&& bd->best_total_moves > 0)
	{
		bd->pos.a = 0;
		if (select_best_move(sd, bd))
			return (error_put(1, "scan_for_best_move : select_best_move()"));
		bd->pos.b--;
	}
	return (0);
}

int	merge_back(t_sdat *sd)
{
	t_bmdat	bd;

	sd->best_push_type = 1;
	while (sd->sta && sd->sta->siz && sd->stb && sd->stb->siz)
	{
		bd = (t_bmdat){(t_pair){0, 0}, (t_pair){0, 0}, (t_pair){0, 0},
			(t_range){0, 0}, (t_range){0, 0}, 0, 0};
		if (update_pos_table(sd))
			return (1);
		if (sd->stb && sd->stb->siz && scan_for_best_move(sd, &bd))
			return (1);
		if (exec_rotate(sd, &bd))
			return (1);
		if (sd->best_push_type == 1 && sd->stb && sd->stb->siz
			&& sd->best_push_type && exec_push(sd))
			return (error_put(1, "scan_for_best_move : exec_push()"));
		else if (sd->best_push_type == 0)
			break ;
	}
	if (sd->stb && sd->stb->siz)
		return (error_put(1, "merge_back : B IS NOT EMPTY"));
	return (0);
}
