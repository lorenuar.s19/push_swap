/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_minimum.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 14:53:01 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/30 13:43:28 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "push_swap.h"

int	find_minimum(t_sdat *sd)
{
	int	i;
	int	min_i;
	int	min;
	int	pos;

	i = 0;
	min_i = -1;
	min = INT_MAX;
	while (i >= 0 && i < sd->inp_siz)
	{
		pos = sd->postab[i].pos;
		if (pos <= min)
		{
			min = pos;
			min_i = i;
		}
		i++;
	}
	return (min_i);
}
