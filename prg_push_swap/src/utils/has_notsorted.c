/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   has_notsorted.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 14:53:01 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/21 23:32:12 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	has_notsorted(t_sdat *sd, t_stack *stack)
{
	int		i;
	int		has_sorted;

	has_sorted = 0;
	i = 0;
	while (i < stack->siz)
	{
		if (get_pos_sorted(sd, stack->dat[i]) == 0)
			has_sorted = 1;
		i++;
	}
	return (has_sorted);
}
