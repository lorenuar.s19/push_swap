/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_print_instr.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 19:46:15 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/10 19:24:09 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	exec_print_instr(t_sdat *sd, int instr)
{
	print_instr(instr);
	return (exec_instr(sd->sta, sd->stb, instr, sd));
}
