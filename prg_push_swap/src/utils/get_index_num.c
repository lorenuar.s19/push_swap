/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_index.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 14:53:01 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/03 18:41:02 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	get_index_num(t_sdat *sd, ssize_t num)
{
	int	i;

	i = 0;
	while (sd && i < sd->inp_siz && sd->postab)
	{
		if (num == sd->postab[i].number)
			return (i);
		i++;
	}
	return (error_printf(-1, "get_index : number not found : %d", num));
}
