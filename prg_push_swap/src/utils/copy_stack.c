/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   copy_stack.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 14:53:01 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/25 16:35:23 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	copy_stack(t_stack *src, t_stack *dest)
{
	int	i;

	i = 0;
	while (src && i < src->siz)
	{
		if (stack_push_back(dest, src->dat[i]))
			return (error_put(1, "copy_stack : stack_push_back()"));
		i++;
	}
	return (0);
}
