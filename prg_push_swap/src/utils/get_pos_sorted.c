/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_pos_sorted.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 14:53:01 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/09 12:54:16 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	get_pos_sorted(t_sdat *sd, ssize_t num)
{
	int	i;

	i = get_index_num(sd, num);
	if (i < 0 || i >= sd->inp_siz)
		return (error_put(-1, "get_move_to_b : invalid index"));
	return (sd->postab[i].sorted);
}
