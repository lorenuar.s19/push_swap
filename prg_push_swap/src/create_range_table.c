/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_range_table.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/04/04 18:18:15 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/21 22:27:16 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "push_swap.h"

static void	sub_create_range_table(t_sdat *sd)
{
	int	i;
	int	res;

	i = 0;
	res = sd->siz.a / sd->range_i_max;
	while (i < sd->range_i_max)
	{
		sd->range_table[i].min = res * i;
		sd->range_table[i].max = res * (i + 1);
		if (i == sd->range_i_max - 1)
		{
			sd->range_table[i].max = sd->siz.a - 1;
		}
		i++;
	}
}

static size_t	log_exp(double base, double val)
{
	if (base <= 1.0 || val <= 1.0)
	{
		return (0);
	}
	return (1 + log_exp(base, val / base));
}

t_range	*create_range_table(t_sdat *sd)
{
	sd->siz.a = stack_get_size(sd->sta);
	sd->range_i_max = 1 + log_exp(1.3, sd->siz.a / 50.0);
	if (sd->range_i_max >= 2)
		sd->range_i_max--;
	sd->range_table = malloc(sd->range_i_max * sizeof(t_range));
	if (!sd->range_table)
	{
		free_sorter_data(sd);
		return (error_ptr_put(E_CRT0));
	}
	if (sd->range_i_max <= 0)
	{
		return (error_ptr_put(E_CRT1));
	}
	sub_create_range_table(sd);
	return (sd->range_table);
}
