/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split_to_b.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/21 14:53:01 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/25 16:14:37 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	sub_sort_three(t_sdat *sd)
{
	while (sd->sta && sd->sta->siz < 4 && stack_check_sorted_ascending(sd->sta))
	{
		if (sd->sta->siz > 1 && sd->sta->dat[0] > sd->sta->dat[1])
		{
			if (exec_print_instr(sd, I_SA))
				return (1);
		}
		if (sd->sta->siz > 2 && sd->sta->dat[1] > sd->sta->dat[2])
		{
			if (exec_print_instr(sd, I_RRA))
				return (1);
		}
	}
	return (0);
}

static int	sub_get_next_split(t_sdat *sd, int moves)
{
	while (moves)
	{
		if (moves > 0)
		{
			if (exec_print_instr(sd, I_RA))
				return (error_put(1, "get_next_split : I_RA"));
			moves--;
		}
		else if (moves < 0)
		{
			if (exec_print_instr(sd, I_RRA))
				return (error_put(1, "get_next_split : I_RRA"));
			moves++;
		}
	}
	return (0);
}

static int	sub_next_split(t_sdat *sd)
{
	int	half_siz;
	int	moves;
	int	i;

	half_siz = sd->siz.a / 2;
	i = sd->siz.a - 1;
	moves = 0;
	while (i >= half_siz && i < sd->siz.a)
	{
		moves--;
		if (sd->postab[i].sorted == 0)
		{
			if (sub_get_next_split(sd, moves))
			{
				return (1);
			}
			return (0);
		}
		i--;
	}
	return (0);
}

static int	get_next_split(t_sdat *sd)
{
	int	half_siz;
	int	moves;
	int	i;

	half_siz = sd->siz.a / 2;
	moves = 0;
	i = 0;
	while (i >= 0 && i < half_siz)
	{
		if (sd->postab[i].sorted == 0)
		{
			if (sub_get_next_split(sd, moves))
			{
				return (1);
			}
			return (0);
		}
		moves++;
		i++;
	}
	if (sub_next_split(sd))
		return (error_put(1, "get_next_split : sub_next_split()"));
	return (0);
}

int	split_to_b(t_sdat *sd, t_stack *lis)
{
	int	splitted;

	if (sd->stb && sd->stb->siz)
		return (error_put(1, "split_to_b : cannot split, b not empty"));
	splitted = 0;
	while (sd->inp_siz > 3 && splitted < (sd->inp_siz - 3 )
		&& has_notsorted(sd, sd->sta))
	{
		if (update_pos_table(sd))
			return (error_put(1, "split_to_b : update_pos_table()"));
		if (update_split(sd, lis))
			return (error_put(1, "split_to_b : update_split()"));
		if (get_next_split(sd))
			return (error_put(1, "split_to_b : get_next_split()"));
		if (sd->sta && sd->postab[0].sorted == 0)
		{
			if (exec_print_instr(sd, I_PB))
				return (error_put(1, E_STB0));
			splitted++;
		}
	}
	if (sub_sort_three(sd))
		return (error_put(1, E_STB3));
	return (0);
}
