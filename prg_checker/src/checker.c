/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/15 17:03:30 by lorenuar          #+#    #+#             */
/*   Updated: 2021/06/25 10:41:23 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static int	sub_checker_loop(t_stack *a, t_stack *b, int verbose)
{
	t_sdat	sd;
	int		ret;
	int		instr;

	sd = (t_sdat){a, b, NULL, (t_range){0, 0}, 0,
		NULL, (t_pair){0, 0}, 0, 0, 0, verbose};
	while (1)
	{
		instr = read_instrs(0);
		if (instr == I_NVALID)
			break ;
		ret = exec_instr(a, b, instr, &sd);
		if (ret)
			return (error_put(ret, E_SCL0));
	}
	return (0);
}

static int	sub_checker_end(t_stack *a, t_stack *b)
{
	if (b->siz || stack_check_sorted_ascending(a))
	{
		stacks_free_all(a, b);
		put_str("KO\n");
		return (1);
	}
	stacks_free_all(a, b);
	put_str("OK\n");
	return (0);
}

static int	checker(int argc, char *argv[], int verbose)
{
	t_stack	a;
	t_stack	b;

	a = (t_stack){NULL, 0, 0};
	b = (t_stack){NULL, 0, 0};
	if (stacks_parse_args(argc, argv, &a, verbose))
	{
		stacks_free_all(&a, &b);
		return (error_put(1, E_C0));
	}
	if (stack_check_valid(&a))
		return (error_put(1, E_C1));
	print_stacks(&a, &b, I_NVALID, verbose);
	if (sub_checker_loop(&a, &b, verbose))
	{
		stacks_free_all(&a, &b);
		return (1);
	}
	return (sub_checker_end(&a, &b));
}

int	main(int argc, char *argv[])
{
	if (argc >= 2)
	{
		return (checker(argc, argv, parse_opts(argc, argv)));
	}
	return (0);
}
